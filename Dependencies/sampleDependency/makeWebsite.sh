#!/bin/bash

cp -r style website/html

# Make list of links
myString="<li> <a href=\"../index.html\">Home</a></li>"
myStringForIndex="<li> <a href=\"./index.html\">Home</a></li>"
cd markdown
for m in *.md; do
  myString="$myString<li> <a href=\"./${m%.md}.html\"> ${m%.md} </a></li>"
  myStringForIndex="$myStringForIndex<li> <a href=\"./html/${m%.md}.html\"> ${m%.md} </a></li>"
done
cd ../
mdrf "Readme.md"
pandoc -V pdfFile="./pdf/Readme.pdf" -V otherFiles="$myStringForIndex"  -M title="$m" --template ./myPandocTemp.html --mathjax "ReadmeRef.md"  -s -o "./website/index.html" 
pandoc "ReadmePrint.md" -s -o "Readme.tex"
pdflatex "Readme.tex"
mv "Readme.pdf" "./website/pdf/"
rm *Ref.md
rm *.log
rm *Print.md
rm *.tex
rm *.aux
rm *.out
 

rm "ReadmeRef.md"
cd markdown
for m in *.md; do
  cd ../mdrfOut
  mdrf "../markdown/$m"
  ls ../
  pandoc -V pdfFile="../pdf/${m%.md}.pdf" -V otherFiles="$myString"  -M title="$m" --template ../myPandocTemp.html --mathjax "${m%.md}Ref.md"  -s -o "${m%.md}.html" 
  pandoc "${m%.md}Print.md" -s -o "${m%.md}.tex"
  pdflatex "${m%.md}.tex"
done
  cd ../
cp mdrfOut/*.html website/html
cp mdrfOut/*.pdf website/pdf

