---
title: " sampleFile"
author : "Jezri Krinsky "
startTime: "23 July 2019"
---

# Imports

import sampleDependency/markdown/sampleFile.md as sd

# Main

## Code test

```python
print ("Hello world")
```
```{.python}
print ("Hello world")
```
```haskell

main::IO ()
main = do
  print "Hello World"

```

```markdown
# Main

Some Text
```

Sample markdown file

## Picture Test

![MyPicture](../pics/pic.jpeg)


## Math Test

$$\frac{\alpha}{\beta}$$

## Reference Test

## ref(sd:Main)

```haskell
1 + 3 + 5 (\x -> 5 + x)
```
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTQxNTI5NzAwNV19
-->