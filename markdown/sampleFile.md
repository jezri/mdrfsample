---
title: " sampleFile"
author : "Jezri Krinsky "
startTime: "23 July 2019"
---

# Imports

import package: (mdrfTest) document: (testDoc)  as: (sd)

# Main

## Code test

```python
print ("Hello world")
```
```{.python}
print ("Hello world")
```
```haskell

main::IO ()
main = do
  print "Hello World"

```

```markdown
# Main

Some Text
```

Sample markdown file

## Picture Test

![MyPicture](../pics/pic.jpeg)


## Math Test

$$\frac{\alpha}{\beta}$$

## Reference Test

## ref(sd:Main)

## Code test

```haskell
(\x y -> x . y $ y) (4 +) (5*)
```
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTU3NzU1MjE1Ml19
-->