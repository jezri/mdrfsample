---
title: " Readme"
author : "Jezri Krinsky "
startTime: "23 July 2019"
---

# Imports

# Main

[Here is a link to a tarziped gbal of the package, download and unzip to ~/.mdrfDocuments to use](test_mdrf.tar.gz)


This is the main documentation for building websites with referenced markdown.

## Installation vs use through git vs use as member of a team

Currently the software only runs in its completeness on some distros of linux. However it is quite possible for people not using to contribute to websites and even maintain their own websites with just a few alterations. This Document will be broken down into contributing to existing websites followed by the creation of new websites (As the script to create new website although simple and probability trivial to adapt is not yet compatible with other operating systems and is probably a smaller user case).

## Editing existing website

* All edits should be submitted to a central repository should be done of git.
* The only files that should be edited regularly is

### Files in markdown


These are the main files which will be displayed the vast majority of the format for these files can be found in the documentation for pandoc markdown only the exceptions to these are given here.

#### Yaml or metadata

Yaml or metadata is not used as of yet though it is  intended to be implemented

####  Use of hashs at the beginning of lines

This is not allowed unless it is for headings. Ie it will couse errors if used in code blocks ect.

#### Only headings with hashs and no ids are allowed

The program assigns its own ids to heading so no other ids are allowed

#### All files not used as libraries must have a section titled Main

#### Pictures folder

All pictures in folders under markdown must be referd to with 
```markdown
../pics/<path from pics>`
```
 and in the Readme from `
 ```markdown
./pics<file from pics>`

```
 Modules called from readme is not recommended and will not have working pictures as of yet.

#### extensions

##### Reference headings

if a heading starts with ref followed by a string a string of the format

```markdown
 ([path to disiread section seperated by ;];<section name>)
```
For example

```markdown 

# Section

Some text under Section

## SubSection

Some text under sub section

# ref(Section;SubSection)

```

##### Inline references

This can be done with the same command but without the heading ie


```markdown 

 # Section

  Some text under Section

 ## SubSection

  Some text under sub section

 # Section B
 
 Some text ref(Section;SubSection)

```

##### Appendix references

This can be done in text and begin with `appRef` but otherwise have the same format


```markdown 

 # Section

  Some text under Section

 ## SubSection

  Some text under sub section

 # Section B
 
 Some text appRef(Section;SubSection)

```

##### Hidden headings

These are useful for when one wants expandable sections interspersed with text.

```markdown 

 # hide(Section)

  Some text under Section

```

##### Importing other Documents

Add an initial section to the Document titled Imports followed by a list of lines of imports as follows


```markdown

# Imports

import <filepath> as <sudoname>
import package: (fooPackage) document: (fooDoc) as: (foo)
import package: (barPackage) document: (barPackage) as: (bar)
```

Section from imports are always available but to remove uncertainty and possibly to improve performance the sudonames can be used before referenced followed by a semi colon.

```markdown
 
 # ref(f:Foo Section; Foo subsection)

```

The program will scan the current directory as well as the Documents directory to find the imported file. However the full contents of dependencies is moved to the documents directory before the website is assembled on gitlab so this directory can also be used the imported file. However the full contents of dependencies is moved to the documents directory before the website is assembled on gitlab so this directory can also be used.

### Pictures in pics

All picture should be stored under pics under a sub-directory named after the package they belong to. This must be done manually at present.

### Extra packages in Dependencies

This must be done manually as it is difficult to use git from a docker box. A more advanced package manager is needed at some point

## Setting up a website 

Clone this directory and run `install.sh` then to create a new website run `makeMdrfSite.sh` followed by the name of the new website. This creates a new directory. Open the directory and connect it with a git lab repository following the instructions on git lab.

## Local use

If the `mdrf` binary is compatible with your operating system or you rebuild it using Haskell stack this is an option. 

The easiest way to do this is by running the `makeWebsite.sh` script in a folder to generate the website locally.

mdrf is also usable and genrates 2 other markdown files ` <foo>Ref.md`  and `<foo>Print.md` These must be converted to other formats with pandoc using chosen templates. The ref file contains javascript functions to fold all sections and the Print file has all duplicates removed.
	

<!--stackedit_data:
eyJoaXN0b3J5IjpbMTE1OTM2MDY3NSwtMzMwNjE5NDY4XX0=
-->
