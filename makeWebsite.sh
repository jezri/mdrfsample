#!/bin/bash

if [ ! -d mdrfOut ]; then mkdir mdrfOut; fi;
if [ ! -d website ]; then mkdir website; fi;
if [ ! -d website/html ]; then mkdir website/html; fi;
if [ ! -d website/pdf ]; then mkdir website/pdf; fi;
cp style/mdrf.css website/html
if [ ! -d pics ]; then mkdir pics; fi;


# Copy and convert docx
if [ -d docx ]; 
  then 
    cd docx
    ls
    for d in *.docx; do
      pandoc --extract-media=../pics --atx-headers  $d -s -o ../markdown/${d%.docx}.md
    done;
    cd ../
fi;

if [ -d rmarkdown ];
  then 
    cd rmarkdown
    ls
    for r in *.Rmd; do
      echo $r
      echo "library(knitr)" >temp.r
      echo "knit(\"$r\")" >> temp.r
      Rscript temp.r
      pandoc --extract-media=../pics --atx-headers ${r%.Rmd}.md -s -o  ${r%.Rmd}.md
      cp *.md ../markdown
    done;
    cd ../
fi
# Make list of links
myString="<li> <a href=\"../index.html\">Home</a></li>"
myStringForIndex="<li> <a href=\"./index.html\">Home</a></li>"
cd markdown
for m in *.md; do
  myString="$myString<li> <a href=\"./${m%.md}.html\"> ${m%.md} </a></li>"
  myStringForIndex="$myStringForIndex<li> <a href=\"./html/${m%.md}.html\"> ${m%.md} </a></li>"
done
cd ../
mdrf "Readme.md"
pandoc -V myCss="./html/mdrf.css" -V pdfFile="./pdf/Readme.pdf" -V otherFiles="$myStringForIndex"  -M title="$m" --template ./myPandocTemp.html --mathjax "ReadmeRef.md"  -s  --highlight-style=./dracula.theme -o "./website/index.html" 

pandoc "ReadmePrint.md" -s  -o "Readme.tex"
pdflatex -interaction=nonstopmode "Readme.tex"
mv "Readme.pdf" "./website/pdf/"
rm *Ref.md
rm *.log
rm *Print.md
rm *.tex
rm *.aux
rm *.out
 

rm "ReadmeRef.md"
cd markdown
for m in *.md; do
  cd ../mdrfOut
  mdrf "../markdown/$m"
  ls ../
  pandoc  -V myCss="./mdrf.css" -V pdfFile="../pdf/${m%.md}.pdf" -V otherFiles="$myString"  -M title="$m" --template ../myPandocTemp.html --mathjax "${m%.md}Ref.md"  -s --highlight-style=../dracula.theme -o "${m%.md}.html" 
  # --highlight-style"../dracula.theme"
  echo "Pandoc conversion successful"
  pandoc "${m%.md}Print.md" -s -o "${m%.md}.tex"
  pdflatex -interaction=nonstopmode "${m%.md}.tex"
done
  cd ../
cp mdrfOut/*.html website/html
cp mdrfOut/*.pdf website/pdf
cp -r pics website/
